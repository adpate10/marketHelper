import requests
import json
import urllib2
import ssl
from functools import wraps
def sslwrap(func):
    @wraps(func)
    def bar(*args, **kw):
        kw['ssl_version'] = ssl.PROTOCOL_TLSv1
        return func(*args, **kw)
    return bar

ssl.wrap_socket = sslwrap(ssl.wrap_socket)
def jsonDefault(object):
    return object.__dict__
base_url="https://www.alphavantage.co/query?"

from bs4 import BeautifulSoup
#r = requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_WEEKLY_ADJUSTED&symbol=MSFT&apikey=BC20N0TIEVQCGZW8&')
#IBM Watson stuff
data=json.load(open('MSFT.json'))
print("Close of MSFT April 20th, 2000 is:")
print(data["Weekly Adjusted Time Series"]["2000-04-20"]["4. close"])
BTC=json.load(open('BTC.json'))
print("close of BTC in China March 13th 2015 is:")
print(BTC["Time Series (Digital Currency Daily)"]["2015-03-13"]["4b. close (USD)"])
def totalAverage(data):
    sum=0
    count=0
    for i in data:
        print(i)

        print(int(data[i]["4b. close (USD)"]))
        sum+=int(data[i]["4b. close (USD)"])
        count+=1
    avg=sum/count
#print(totalAverage(BTC["Time Series (Digital Currency Daily)"]))

url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=MSFT&apikey=BC20N0TIEVQCGZW8&.json"
#cat=(urllib2.urlopen(url))